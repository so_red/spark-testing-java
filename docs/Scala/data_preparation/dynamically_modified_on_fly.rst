==========================================
Dynamically modified on fly
==========================================

If input data structure is complex, and only several fields take part in transformation, presaved data can be read, and fields modified on fly.

.. code-block:: scala

      test("Mass When weight specified Then weights sum") {
        val expected = 85
        val input = getBulkData.withColumn("weight", lit(85))
        repository.mass(input) shouldBe expected
      }

      test("Mass When weight is null Then NPE") {
        val input = getBulkData.withColumn("weight", lit(null).cast(DataTypes.IntegerType))
        intercept[RuntimeException] {
          repository.mass(input)
        }

      }

      private def getBulkData = spark.read.option("header", "true").csv(getTestDataFolder)

Example: `DynamicallyModifiedOnFlySpec.scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/data_preparation/DynamicallyModifiedOnFlySpec.scala>`_
