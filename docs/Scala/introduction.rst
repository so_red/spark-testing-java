==========================================
Scala
==========================================
ScalaTest `FunSuite <http://www.scalatest.org/getting_started_with_fun_suite>`_ is used as testing framework.

Project with code examples on GitLab: `spark-testing-scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala>`_

Functionality located in package `"repository" <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/tree/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/repository>`_

.. toctree::
    :hidden:

    Context creation <context_creation/context_creation.rst>
    Data preparation <data_preparation/data_preparation.rst>
    Comparison <comparison.rst>
