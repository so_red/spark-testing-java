==========================================
Comparison
==========================================

One value - with "first"
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala
        :emphasize-lines: 3

        val apple = Apple("Green", 85)
        val df = List(apple).toDF()
        val actual: Int = df.first.getAs("weight")

        actual shouldEqual apple.weight


Primitives list - with "as"
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala
        :emphasize-lines: 5

            val df = List("Green", "Red").toDF("color")
            val actual = df.select("colour").as(Encoders.STRING).collect()


DataFrames - with "checkAnswer"
"""""""""""""""""""""""""""""""""""""

.. code-block:: scala
        :emphasize-lines: 5

        val apples = List(Apple("Green", 85))
        val expected = apples.toDF()
        val actual = apples.toDF()

        checkAnswer(expected, actual)


Example: `ComparisonSpec.scala <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-scala/blob/master/src/test/scala/org/zcs/spike/server/spark/testing/scala/comparison/ComparisonSpec.scala>`_

