==========================================
DataFrame (Dataset<Row> in Java)
==========================================

Empty with predefined structure
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        final Dataset<Row> actual = spark().emptyDataFrame().withColumn("color", lit("green"));
        actual.printSchema();

Single primitive
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        Long value = 12L;
        List<Row> rows = Collections.singletonList(RowFactory.create(value));
        final Dataset<Row> actual = spark().createDataFrame(rows, Encoders.LONG().schema());

Primitive list
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        List<String> data = Lists.newArrayList("green", "red");
        Dataset<Row> actual = spark().createDataset(data, Encoders.STRING()).toDF("color");

Row list with assigned schema
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        List<Row> rows = Arrays.asList(RowFactory.create("green"), RowFactory.create("red"));
        StructType schema = DataTypes.createStructType(
                new StructField[]{DataTypes.createStructField("color", DataTypes.StringType, false)});

        final Dataset<Row> actual = spark().createDataFrame(rows, schema);


List of entities
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        List<Apple> rows = Lists.newArrayList(new Apple("green", 70), new Apple("red", 110));
        final Dataset<Row> actual = spark().createDataFrame(rows, Apple.class);


Example: `DataFrameCreationTest.java <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/blob/master/src/test/java/org/zcs/spike/server/spark/testing/java/data_preparation/DataFrameCreationTest.java>`_