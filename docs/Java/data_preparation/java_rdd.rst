==========================================
JavaRDD
==========================================

Empty
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        jsc().emptyRDD();

From primitive list
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        List<String> data = Lists.newArrayList("green", "red");
        jsc().parallelize(data);

From entity list
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        List<Apple> rows = Lists.newArrayList(new Apple("green", 70), new Apple("red", 110));
        JavaRDD<Apple> result = jsc().parallelize(rows);

Example: `RDDCreationTest.java <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/blob/master/src/test/java/org/zcs/spike/server/spark/testing/java/data_preparation/RDDCreationTest.java>`_