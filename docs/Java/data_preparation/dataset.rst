==========================================
Dataset
==========================================

From list of entities
"""""""""""""""""""""""""""""""""""""

.. code-block:: java

        List<Apple> rows = Lists.newArrayList(new Apple("green", 70), new Apple("red", 110));
        final Dataset<Apple> actual = spark().createDataset(rows, Encoders.bean(Apple.class));

Example: `DataSetCreationTest.java <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/blob/master/src/test/java/org/zcs/spike/server/spark/testing/java/data_preparation/DataSetCreationTest.java>`_