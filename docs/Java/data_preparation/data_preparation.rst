==========================================
Data preparation
==========================================
Code examples in package: `data_preparation <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/tree/master/src/test/java/org/zcs/spike/server/spark/testing/java/data_preparation>`_

.. toctree::
    :hidden:

    JavaRDD <java_rdd.rst>
    DataFrame <dataframe.rst>
    Dataset <dataset.rst>
    Dynamically on fly <dynamically_modified_on_fly.rst>
