==========================================
Manual
==========================================
Asterix means all available processor cores will be used. For most cases two cores is enough, and "local[2]" is appropriate.

.. code-block:: java
        :emphasize-lines: 2

        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName("test")
                .set("spark.ui.enabled", "false");

        JavaSparkContext jsc = new JavaSparkContext(conf);
        // close after usage
        jsc.stop();

Code example: `JavaSparkContextCreationTest.java <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/blob/master/src/test/java/org/zcs/spike/server/spark/testing/java/context/manual/JavaSparkContextCreationTest.java>`_