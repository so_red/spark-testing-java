==========================================
Context creation
==========================================
Code examples in package: `context <https://gitlab.com/org.zcs.spike.server.spark/spark-testing-java/tree/master/src/test/java/org/zcs/spike/server/spark/testing/java/context>`_

.. toctree::
    :hidden:

    manual.rst
    spark_testing_base.rst
