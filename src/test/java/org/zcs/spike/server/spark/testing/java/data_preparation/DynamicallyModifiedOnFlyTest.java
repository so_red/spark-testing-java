package org.zcs.spike.server.spark.testing.java.data_preparation;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.junit.Test;
import org.zcs.spike.server.spark.testing.java.context.manual.SparkIntegrationTestBase;
import org.zcs.spike.server.spark.testing.java.repository.ApplesRepository;

import java.io.File;

import static org.apache.spark.sql.functions.lit;
import static org.junit.Assert.assertEquals;

public class DynamicallyModifiedOnFlyTest extends SparkIntegrationTestBase {
    private final ApplesRepository repository = new ApplesRepository();

    @Test
    public void testMass_WhenWeightSpecified_ThenWeightsSum() {
        int expected = 85;
        Dataset<Row> input = getBulkData().withColumn("weight", lit(85));
        assertEquals(expected, repository.mass(input));
    }

    @Test(expected = Exception.class)
    public void testMass_WhenWeightIsNull_ThenNPE() {
        Dataset<Row> input = getBulkData().withColumn("weight", lit(null).cast(DataTypes.IntegerType));
        repository.mass(input);
    }

    private Dataset<Row> getBulkData() {
        return spark().read().option("header", "true").csv(getTestDataFolder());
    }

    private String getTestDataFolder() {
        final String dataRoot = "src/test/test-data";
        final String testSubPath = this.getClass().getName().replaceAll("\\.", "/");
        return new File(dataRoot, testSubPath).getPath();
    }
}
